# This is Not FizzBuzz

Write a program that prints all the numbers from 1 to 100. However, for multiples of 3, instead of the number, print "Foo". For multiples of 5 print "Bar". For numbers which are multiples of both 3 and 5, print "Foobar".

# How to Run

Using node v11.10.0

> node index

# How to Test

> node foobar.test