const {Rule, FooBar} = require('./foobar');

const fooBar = new FooBar(100, [
  new Rule((number) => number % 5 === 0 && number % 3 === 0, 'Foobar'),
  new Rule((number) => number % 5 === 0, 'Bar'),
  new Rule((number) => number % 3 === 0, 'Foo'),
]);

console.log(fooBar.print());