class Rule {
  constructor (evaluator, message) {
    this.evaluator = evaluator
    this.message = message
  }

  getMessage () {
    return this.message
  }

  evaluate (number) {
    return this.evaluator(number)
  }
}

class FooBar {
  constructor (limit = 100, rules = []) {
    this.limit = limit
    this.rules = rules
  }

  evaluate (number) {
    // First rule to be truthy will be use to print
    const rule = this.rules.find((rule) => rule.evaluate(number))
    if (rule) {
      return rule.getMessage()
    }
    // or we just return the number
    return `${number}`
  }

  print () {
    const lines = []
    for (let i = 0; i < this.limit; i++) {
      lines.push(this.evaluate(i + 1))
    }
    return lines.join('\n')
  }
}

module.exports = {
  Rule,
  FooBar
}