const { Rule, FooBar } = require('./foobar')

const assert = require('assert').strict;

const rule = new Rule((number) => number % 3 === 0, 'Hello World')

assert.strictEqual(true, rule.evaluate(3), 'Rule evaluates correctly')

assert.strictEqual(false, rule.evaluate(2), 'Rule evaluates correctly')

assert.strictEqual(false, rule.evaluate(1), 'Rule evaluates correctly')

assert.strictEqual(true, rule.evaluate(6), 'Rule evaluates correctly')

assert.strictEqual('Hello World', rule.getMessage(), 'Rule return the correct message')

assert.notStrictEqual('Hello World 2', rule.getMessage(), 'Rule return the correct message')

const fooBarOne = new FooBar(1)

assert.strictEqual('1', fooBarOne.print(), 'FooBar prints the correct numbers')

const fooBarTwo = new FooBar(2)

assert.strictEqual('1\n2', fooBarTwo.print(), 'FooBar prints the correct numbers')

assert.notStrictEqual('1\n2\n3', fooBarTwo.print(), 'FooBar prints the correct numbers')

const fooBarThree = new FooBar(10)

assert.strictEqual(
  Array.from(Array(10).keys()).map(x => x + 1).join('\n'),
  fooBarThree.print(),
  'FooBar prints the correct numbers'
)

assert.notStrictEqual('1\n2\n3', fooBarThree.print(), 'FooBar prints the correct numbers')

const fooBarFour = new FooBar(2, [
  new Rule(number => number % 2 === 0, 'Quack')
])

assert.strictEqual('1\nQuack', fooBarFour.print(), 'FooBar uses rules assigned')

assert.notStrictEqual('1\nQuack2', fooBarFour.print(), 'FooBar uses rules assigned')

console.log('All tests passed.')